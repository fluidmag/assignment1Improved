/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package MakingDice;

/**
 *
 * @author cha_xi
 */
public class RollingDie {
    private final TheDie die1;
    private final TheDie die2;
    

    public RollingDie() {
       die1 = new TheDie();
       die2 = new TheDie();
    }


    
    public void rollingDice(){
        die1.rollTheDie();
        die2.rollTheDie();   
    }
    
    public int sumTwoDice(){
        return die1.getOneDie()+die2.getOneDie();
    }

    @Override
    public String toString() {
        return  "(" + die1.getOneDie() + "," + die2.getOneDie()+")";
    }

    public int getDie1() {
        return die1.getOneDie();
    }

    public int getDie2() {
        return die2.getOneDie();
    }

    
}

