/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Presentation;

import Bankroll.Bankroll;
import java.util.Scanner;
import jraps.*;

/**
 *
 * @author cha_xi
 */
public class Presentation {

    private JRapsAnySeven  playAS;
    private JRapsFieldBet playFB;
    private JRapsPassLine playPL;
    private int moose;
    private Bankroll bc;

    public Presentation(JRapsAnySeven playAS, JRapsFieldBet playFB, JRapsPassLine playPL, Bankroll bc) {
        this.playAS = playAS;
        this.playFB = playFB;
        this.playPL = playPL;
        this.bc = bc;
    }

    public Presentation(Bankroll bc) {
        this.bc = bc;
        playAS = new JRapsAnySeven(bc);
        playFB = new JRapsFieldBet(bc);
        playPL = new JRapsPassLine(bc);
    }

    public Presentation() {
        this.bc = new Bankroll(0);
        playAS = new JRapsAnySeven(bc);
        playFB = new JRapsFieldBet(bc);
        playPL = new JRapsPassLine(bc);
    }

    public int inputChoice() {
        int number = 0;
        String str = "input a Choice, please";
        Scanner sc = new Scanner(System.in);
        while (number <= 0) {
            System.out.println(str);
            while (!sc.hasNextInt()) {
                System.out.println("not valid input, input again");
                sc = new Scanner(System.in);
            }
            number = sc.nextInt();
            str = "not nonNegative number, input again";
        }
        return number;
    }

    public int inputGameChoice() {
        int number = 0;
        String str = "input a Choice, please";
        Scanner sc = new Scanner(System.in);
        while ((number != 1) & (number != 2) & (number != 3)) {
            System.out.println(str);
            while (!sc.hasNextInt()) {
                System.out.println("not valid input, input again");
                sc = new Scanner(System.in);
            }
            number = sc.nextInt();
            str = "not 1,2,3, input again";
        }
        return number;
    }

    //

    public void perform() {
        int i = -1;
        String str = "select game: 1. Any7, 2.FieldBet, 3. PassLine";

        do {
            //select game
            System.out.println(str);
            i = this.inputGameChoice();
            System.out.println("*********one turn begin*************************");
            //
            if (i == 1) {
                playAS.perform();
            } else if (i == 2) {
                playFB.perform();
            } else if (i == 3) {
                playPL.perform();
            } else {
                System.exit(0);
            }
            // whether to continue; 
            System.out.println("************one turn over**************************");
            System.out.println("Continue play, input 1,else other positive number");
            i = this.inputChoice();
        } while (i == 1);
    }

}
